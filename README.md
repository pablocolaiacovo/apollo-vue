# Vue + Apollo Middleware

- [x] Setup Apollo Server
- [x] Setup Vue SPA
- [x] Connect
- [x] Deploy

## Apollo Server

```sh
$ cd apollo && npm run start
```

https://dockerized-apollo-server.azurewebsites.net/

## Vue SPA

Run

```sh
$ cd vue-spa && npm run serve
```

Build

```sh
$ cd vue-spa && npm run build
```

https://vueapollospa.z13.web.core.windows.net/

## Unsplash Images

Set the `vue-spa/.env.local` file with the corresponding Unslpash key:

```
VUE_APP_UNSPLASH_ACCESS_KEY=
```
